import * as mongoose from 'mongoose';

export const trainingSchema = new mongoose.Schema({
    description: {
        String,
        required: false,
    },
    iconUrl: {
        String,
        required: false,
    },
    courseListIcon: {
        String,
        required: false,
    },
    longDescription: {
        String,
        required: false,
    },
    lasts: {
        String,
        required: false,
    },
    requirements: {
        String,
        required: false,
    },
    content: [
        {
            String,
            required: false,
        },
    ],
});

