import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import {join} from 'path';
import * as path from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
      AppModule,
      new FastifyAdapter(),
  );

/*  app.useStaticAssets({
    root: join(__dirname, '..', 'public'),
    prefix: '/public/',
  });*/
  app.setViewEngine({
    engine: {
      handlebars: require('handlebars'),
    },
    templates: join(__dirname, '..', 'views'),
  });

  app.enableCors({
    credentials: true,
    origin: (origin, callback) => {
      // console.log(`Req - ${origin}`);
      const whitelist = [
        'http://127.0.0.1:3000',
        'http://127.0.0.1:4200',
        'http://localhost:3000',
        'http://localhost:4200',
      ];

      if(whitelist.includes(origin) || !origin){
        // console.log(`Req - ${origin}`);
        callback(null, true);
      } else {
        // console.log(`Req - ${origin}`);
        callback(new Error('Not allowed by CORS'));
      }

    },
  });

  await app.listen(3000);
}
bootstrap();



/*async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
/!*  const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
  };*!/
  // app.use(cors(corsOptions));
  app.enableCors({
    credentials: true,
    origin: (origin, callback) => {
      // console.log(`Req - ${origin}`);
      const whitelist = [
        'http://127.0.0.1:3000',
        'http://127.0.0.1:4200',
        'http://localhost:3000',
        'http://localhost:4200',
      ];

      if(whitelist.includes(origin) || !origin){
        // console.log(`Req - ${origin}`);
        callback(null, true);
      } else {
        // console.log(`Req - ${origin}`);
        callback(new Error('Not allowed by CORS'));
      }

    },
  });
  app.use(helmet());
  app.useStaticAssets(join(__dirname, '..', 'public'));
  await app.listen(3000);
}
bootstrap();*/
