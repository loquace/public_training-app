import {Controller, Get, Param, Render, Res} from '@nestjs/common';
import {AppService} from './app.service';


@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {
    }

    @Get()
    getHello(): string {
        return this.appService.getHello();
    }

    @Get('test')
    @Render('index.hbs')
    test() {
        return { message: 'Test ok' };
    }

    /*@Get('track/:imgId')
    test(@Param('imgId') imgId, request, reply) {
        const fs = require('fs');
        fs.realpath(imgId);
        const stream = fs.createReadStream('some-file', 'utf8');
        reply.send(stream);
    }*/
}
