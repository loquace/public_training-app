import {Module } from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {MongooseModule} from '@nestjs/mongoose';
import {CatalogModule} from './catalog/catalog.module';

@Module({
    imports: [
        MongooseModule.forRoot(
            'connect_chain_here',
            {
                useNewUrlParser: true,
            },
        ),
        CatalogModule],
    controllers: [AppController],
    providers: [
        AppService,
    ],
})
export class AppModule {

}
