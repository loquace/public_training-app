import {Controller, Get, HttpStatus, NotFoundException, Param, Req, Res} from '@nestjs/common';
import {CatalogService} from './catalog.service';
import * as mongoose from 'mongoose';

@Controller('catalog')
export class CatalogController {

    constructor(
        private catalogService: CatalogService,
    ) {
    }

    /*@Get('list')
    async findAll(@Res() res) {
        const trainings = await this.catalogService.findAll();
        return res.status(HttpStatus.OK).json(trainings);
    }

    @Get('training/:id')
    async findOne(@Res() res, @Param('id', new mongoose.Types.ObjectId()) id) {
        const training = await this.catalogService.findOne(id);
        if (!training) {
            throw new NotFoundException('Formation inexistante');
        }
        return res.status(HttpStatus.OK).json(training);
    }*/

    /* fastify*/
    @Get('list')
    async findAll() {
        return await this.catalogService.findAll();
    }

    @Get('training/:id')
    async findOne(@Param('id', new mongoose.Types.ObjectId()) id) {
      return await this.catalogService.findOne(id);
    }
}
