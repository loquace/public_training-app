import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { trainingSchema } from '../schemas/training.schema';
import { CatalogController } from './catalog.controller';
import { CatalogService } from './catalog.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Trainings', schema: trainingSchema }])],
    controllers: [CatalogController],
    providers: [CatalogService],
})
export class CatalogModule {}
