export class CreateTrainingDto {
    readonly description: string;
    readonly iconUrl: string;
    readonly courseListIcon: string;
    readonly longDescription: string;
    readonly lasts: string;
    readonly requirements: string;
    readonly content: [];
}
