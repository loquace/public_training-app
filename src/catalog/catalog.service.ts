import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Trainings } from './interfaces/training.interface';
import { CreateTrainingDto } from './dto/create-training.dto';

@Injectable()
export class CatalogService {

    constructor(@InjectModel('Trainings') private readonly trainingModel: Model<Trainings>) {}

    async create(createTrainingDto: CreateTrainingDto): Promise<Trainings> {
        const createTraining = new this.trainingModel(createTrainingDto);
        return await createTraining.save();
    }

    async findAll(): Promise<Trainings[]> {
        const trainings =  await this.trainingModel.find().exec();
        // console.log(trainings);
        return trainings;
    }

    async findOne(id): Promise<Trainings> {
        const training = await this.trainingModel
            .findById(id)
            .exec();
        return training;
    }

}
